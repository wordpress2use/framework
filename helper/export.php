<?php

require_once('wp-config.php');

function wp_backup_mysql($dbhost,$dbusername,$dbpassword,$dbname,$tables = '*'){
    $db = new mysqli($dbhost, $dbusername, $dbpassword, $dbname); 

    if($tables == '*') { 
        $tables = array();
        $result = $db->query("SHOW TABLES");
        while($row = $result->fetch_row()) { 
            $tables[] = $row[0];
        }
    } else { 
        $tables = is_array($tables)?$tables:explode(',',$tables);
    }

    $return = '';

    foreach($tables as $table){
        $result = $db->query("SELECT * FROM $table");
        $numColumns = $result->field_count;

        /* $return .= "DROP TABLE $table;"; */
        $result2 = $db->query("SHOW CREATE TABLE $table");
        $row2 = $result2->fetch_row();

        $return .= "\n\n".$row2[1].";\n\n";

        for($i = 0; $i < $numColumns; $i++) { 
            while($row = $result->fetch_row()) { 
                $return .= "INSERT INTO $table VALUES(";
                for($j=0; $j < $numColumns; $j++) { 
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = $row[$j];
                    if (isset($row[$j])) { 
                        $return .= '"'.$row[$j].'"' ;
                    } else { 
                        $return .= '""';
                    }
                    if ($j < ($numColumns-1)) {
                        $return.= ',';
                    }
                }
                $return .= ");\n";
            }
        }

        $return .= "\n\n\n";
    }

    if (isset($_GET['file'])) {
        $target = $_GET['file'];

        if (!strlen($target)) {
            $target = 'db_'.DB_NAME.'_'.time().'.sql';
        }
        $handle = fopen($target,'w+');
        fwrite($handle,$return);
        fclose($handle);
        echo "Database Export Successfully!";
    } else {
        header("Content-Type: application/sql");
        header('Content-Disposition: attachment; filename=db_'.DB_NAME.'_'.time().'.sql');
        echo $return;
    }
}

wp_backup_mysql(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);

